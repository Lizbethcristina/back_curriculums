package com.example.curriculums.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "EXPERIENCIA")
public class Experiencia {

	
	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column( name="ID_EXPERIENCIA")
	private Integer idExperiencia;
	
	
	@Column( name="fechaInicio", length = 25)
	public String fechaInicio;
	
	@Column( name="fechaFin", length = 25)
    public String fechaFin;
	
	@Column( name="descripcion", length = 25)
    public String descripcion;
}
