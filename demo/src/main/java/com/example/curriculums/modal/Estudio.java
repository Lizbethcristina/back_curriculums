package com.example.curriculums.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "ESTUDIO")
public class Estudio {

	
	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column( name="ID_ESTUDIO")
	private Integer idEstudio;
	
	@Column( name="nombreEstudio", length = 25)
	public String nombreEstudio;
	
	@Column( name="descripcionEstudio", length = 25)
    public String descripcionEstudio;
	
	@Column( name="fecha", length = 25)
    public String fecha;
}
