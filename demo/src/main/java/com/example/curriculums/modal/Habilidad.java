package com.example.curriculums.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "HABILIDAD")
public class Habilidad {
	
	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column( name="ID_HABILIDAD")
	private Integer idHabilidad;
	

	@Column( name="nombre", length = 25)
	public String nombre;
	
	@Column( name="nivel", length = 25)
    public String nivel;

}
