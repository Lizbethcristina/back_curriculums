package com.example.curriculums.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "CURRICULUM")
public class Curriculum {

	
	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( name="ID_C")
	private Integer idCurriculum;
	
	
	 public Persona persona;
	    public Experiencia[] experiencias;
	    public Estudio[] estudios;
	    public Habilidad[] habilidades;
	    
	    @Column( name="url")
	    public String url;
	    
	    @Column( name="cartaPresentacion")
	    public String cartaPresentacion;
	    
	    @Column( name="imagen")
	    public String imagen;
}
