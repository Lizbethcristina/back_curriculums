package com.example.curriculums.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "PERSONA")
public class Persona {

	
	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column( name="ID_PERSONA")
	private Integer idPersona;
	
	
	@Column( name="nombre", length = 25)
	public String nombre;
	
	@Column( name="apellido", length = 25)
	public String apellido;
	
	@Column( name="telefono", length = 25)
	public String telefono;
	
	@Column( name="correo", length = 25)
	public String correo;
	
	@Column( name="direccion", length = 25)
	public String direccion;
}
