package com.example.curriculums.modal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table( name = "USUARIO")
public class Usuario {

	@Id		
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column( name="ID_USUARIO")
	private Integer idUsuario;
	
	@Column( name="USUARIO", length = 25)
	private String usuario;
	
    @Column(name = "CONTRASEÑA", length = 50)
	private String contraseña;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

}
