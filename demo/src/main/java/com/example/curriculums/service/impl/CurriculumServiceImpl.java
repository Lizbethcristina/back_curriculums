package com.example.curriculums.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.curriculums.modal.Curriculum;
import com.example.curriculums.repo.CurriculumRepo;
import com.example.curriculums.service.ICurriculumService;


@Service
public class CurriculumServiceImpl implements ICurriculumService{

	
	@Autowired
    private CurriculumRepo curriculumRepo;
	
	
	@Override
	public void crearCurriculum(Curriculum c) {
		curriculumRepo.save(c);
	}

}
