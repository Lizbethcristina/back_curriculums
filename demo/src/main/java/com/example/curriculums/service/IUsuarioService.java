package com.example.curriculums.service;
import java.util.List;
import java.util.Optional;

import com.example.curriculums.modal.Usuario;



public interface IUsuarioService {

	List<Usuario> getAllUsuario();
	
	Optional<Usuario> getUsuario(Integer id);
	
	Usuario addUsuario(Usuario usuario);
	
	void updateUsuario(Usuario usuario);
	
	void deleteUsuario(Integer id);	
	
}
