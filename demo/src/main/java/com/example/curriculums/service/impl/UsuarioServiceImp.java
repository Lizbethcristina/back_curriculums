package com.example.curriculums.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.curriculums.modal.Usuario;
import com.example.curriculums.repo.IUsuarioRepo;
import com.example.curriculums.service.IUsuarioService;



@Service
public class UsuarioServiceImp implements IUsuarioService{

	@Autowired
	private IUsuarioRepo loginRepo;
	
	
	
	
	

	@Override
	public Usuario addUsuario(Usuario usuario) {
		return loginRepo.save(usuario);
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		loginRepo.save(usuario);
		
	}

	@Override
	public List<Usuario> getAllUsuario() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Usuario> getUsuario(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUsuario(Integer id) {
		// TODO Auto-generated method stub
		
	}
	
	
}
