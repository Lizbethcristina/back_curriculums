package com.example.curriculums.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.curriculums.modal.Usuario;


public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>{

}
