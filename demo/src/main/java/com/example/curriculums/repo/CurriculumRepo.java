package com.example.curriculums.repo;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.curriculums.modal.Curriculum;

public interface CurriculumRepo extends JpaRepository<Curriculum,Integer>{

}
