package com.example.curriculums.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.curriculums.modal.Usuario;
import com.example.curriculums.service.IUsuarioService;





@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@RequestMapping(value = "/addusuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Usuario addUsuario(@RequestBody Usuario usuario) {
		Usuario newUsuario = null;
		newUsuario = usuarioService.addUsuario(usuario);
		return newUsuario;
	}

	@RequestMapping(value = "/listusuario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Usuario> getUsuario(){
		List<Usuario> list = null;
		list = usuarioService.getAllUsuario();
		return list;
	}
	
	@RequestMapping(value = "/getusuario/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<Usuario> getUsuariobyID(@PathVariable("id") Integer id){
		Optional<Usuario> list = null;
		list = usuarioService.getUsuario(id);
		return list;
	}	
}
