package com.example.curriculums.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.curriculums.modal.Curriculum;
import com.example.curriculums.service.ICurriculumService;





@RestController
public class CurriculumController {
	
	
    public static final Logger LOGGER = LoggerFactory.getLogger(CurriculumController.class);

    @Autowired
    private ICurriculumService curriculumService;

    
    @PostMapping("curriculum/crear")
    public void crearCurriculum(@RequestBody(required = true) Curriculum c) {
        try {
        	 curriculumService.crearCurriculum(c);
           
        
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

    }
    
}
